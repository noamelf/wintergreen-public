"""Majordomo Protocol definitions"""
# This is the version of MDP/Client we implement
C_CLIENT = "MDPC01"

#  This is the version of MDP/Worker we implement
W_WORKER = "MDPW01"

#  MDP/Server commands, as strings
W_READY = "READY"
W_REQUEST = "REQUEST"
W_REPLY = "REPLY"
W_HEARTBEAT = "HEARTBEAT"
W_DISCONNECT = "DISCONNECT"

commands = [None, "READY", "REQUEST", "REPLY", "HEARTBEAT", "DISCONNECT"]
