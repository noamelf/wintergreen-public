"""
Majordomo Protocol client example. Uses the mdcli API to hide all MDP aspects

Author : Min RK <benjaminrk@gmail.com>

"""
import json

import sys
import time
from mdcliapi import MajorDomoClient


def main():
    verbose = '-v' in sys.argv
    client = MajorDomoClient('tcp://localhost:5555', verbose)
    count = 0
    while count < 100000:
        attr = {'product': 'shoe #{}'.format(count)}
        try:
            reply = client.send('checkout', 'USA', 'IOS', json.dumps(attr))
        except KeyboardInterrupt:
            break
        else:  # also break on failure to reply:
            if reply is None:
                break
        count += 1
        time.sleep(10)

        print '%i requests/replies processed' % count

if __name__ == '__main__':
    main()
