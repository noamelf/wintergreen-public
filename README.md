Shoppimon Coding Exercise - the "Wintergreen" messaging system
===============================================================================

Overview
--------
The purpose of this task is to implement a Python / ZeroMQ based message broker
with some specific capabilities. You are encouraged to carefully think about the
design most suitable for the requirements. Pick the right ZeroMQ messaging
patters, and handle failures where it makes sense but not to over-design things
(being practical is important).

It is also recommended to start with module / class / function stubs and
implement the details as you go. A perfect implementation is not required, but
a complete design (as presented by stubs and documentation) is.

You are advised to go over the ZeroMQ documentation - many of the problems you
might be thinking of solving yourself are already solved and presented there.

A complete implementation would include three Python modules with "main"
(executable using `python -m ...`): client.py, broker.py and agent.py. You
should ensure readability. Following PEP8 is highly recommended. Proper
documentation is encouraged.


Requirements
------------
Design an implement a messaging system consisting of 3 parts:

 1. One or more clients, requiring the service of "agents". Clients send
 messages containing commands to agents through a broker, and expect a
 response. There can be 1 or more clients running at the same time.

 A client needs only to know what command it is sending and what capabilities
 and region are required to handle it, and the broker's address. Once a response
 is received from an agent, the client can terminate or move on to the next
 task.

 2. Agents - are in fact "browser" processes with specific capabilities. For
 example, an agent can be capable of running "android", "safari", "ios",
 "chrome" or "firefox" simulations. In addition, each agent is located in one
 of 3 regions: "europe", "usa" or "japan". Note that agents are stateless and
 volatile - they may "appear" or "disappear" without warning due to outages and
 capacity throttling algorithms.

 An agent only needs to know its own capabilities and region, and the broker's
 address. A single agent process can handle multiple commands sequentially (no
concurrency!), and keeps no state between command executions.

3. Broker - there is only one message broker and in fact it is the only "known"
part in the system, in that the only thing clients and agents need to know is
the address of the broker - there is no awareness by any of the other components
to any of the other components. The message broker is responsible for:

 * Keeping track of available agents, their capabilities and location
 * Receiving commands from clients, and pushing them to the right agent, given
   requested capabilities and region.
 * If all agents with requested attributes are busy, queueing commands until one
   becomes ready
 * Detecting agent drop-offs

Important: make sure your design properly handles the fact that agent processes
"come and go as they please" - assume that they are launched by a magical system
that handles scaling them up and down which you are not aware of, and also
destroyed by it. An agent worker may show up and "ask" for work - the broker
or client should not require any reconfiguration or human intervention when
that happens. The same applies when an agent shuts down (which it may notify on)
or even crashes and just "disappears" - the broker should be able to handle such
cases relatively gracefully - with the worst case being that if an agent that
started handling a command has disappeared, this command may be sent again to
another agent after a reasonable timeout.


Don't Worry About...
---------------------
Some things are specifically *not* required:

 * No need to keep track of clients by the broker. If a client crashes, nobody
   cares.
 * Fault tolerance can be achieved by handling timeouts and sending messages
   again. It is not critical if by an odd chance a message is sent and executed
   twice because it was "lost" the first time. It shouldn't be common though.
 * The agent doesn't really need to do anything - sleeping for 10 seconds is
   a "good enough" action to take to simulate actual work.
 * Scaling the number of agents up or down based on the amount of work in the
   queue. Assume this is handled elsewhere.


Request / Response Format
-------------------------
Decide on a format and serialization protocol for messages. Messages should
contain the following information (represented here a Python dictionary):

    {"command": "do_something", # the action to perform, not critical here
     "region": "europe", # one of the available regions
     "capability": "ios", # for now we assume only one capability is required
     "attributes": { # a dictionary of command attributes, can be complex data  
     }
    }

A response returned from the agent after completing the task (all the way to
the requesting client should be of the following format:

    {"command": "do_something", # same command we got
     "status": "ok", # or "error" if something went wrong
     "result": { # a dictionary of data, can be complex
     }
    }

You can safely ignore the values of the "command", "attributes" and "result"
fields, but make sure they are properly serialized and passed.

If you have any questions, feel free to email your contact person at Shoppimon.
Remember to keep things as simple as possible, but not simpler.

Good luck!
