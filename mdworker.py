"""Majordomo Protocol worker example.

Uses the mdwrk API to hide all MDP aspects

Author: Min RK <benjaminrk@gmail.com>
"""

import sys
import time
from mdwrkapi import MajorDomoWorker


def main():
    verbose = '-v' in sys.argv
    worker = MajorDomoWorker('tcp://localhost:5555', 'checkout', 'USA', 'IOS', verbose)
    reply = None
    while True:
        request = worker.recv(reply)
        if request is None:
            break  # Worker was interrupted

        time.sleep(1)  # processing the request

        reply = request


if __name__ == '__main__':
    main()
